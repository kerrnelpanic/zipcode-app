# README #
A snappy zip-code lookup tool, equipped with German zip-codes.  
It features two different search modes and can be backed by a simple text file or a database system.  
Version 1.0.0  
Testet with jdk-11.0.10.9-hotspot
### setup ###
All files contained in  _lib_  must be included in classpath. Entry point can be found in  _ZipMain.java_ .
### contact ###
Baldur Kellner - baldur.kellner@gmail.com